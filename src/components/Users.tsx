import "./Users.css";
import { Component } from "react";
import User from "./User";
import Login from "./Login";
import User1 from "./User1"
import InvalidUser from "./InvalidUser"
import User2 from "./User2"
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Redirect,
} from "react-router-dom";
class Users extends Component<{}, { validName: any, validUserNames: any, username: string, isLog: boolean, isSubmitted: boolean }> {
    constructor(props: any) {
        super(props)

        this.state = {
            username: "",
            validUserNames: ["Ajay",],
            validName: ["Neha"],
            isLog: false,
            isSubmitted: false
        }
    }
    handlerUserameChange = (e: any) => {
        this.setState({
            username: e.target.value
        })
    }
    handleSubmit = () => {
        this.setState({
            isSubmitted: true
        })
    }
    render() {
        return (
            <div className="container">
                {
                    this.state.isSubmitted ?
                        <Login >
                            <Router>
                                <Switch >
                                    <Route path='/Other' exact component={InvalidUser} />
                                    <Route path='/Ajay' exact component={User2} />
                                    <Route path='/Neha' exact component={User1} />
                                    {this.state.validUserNames.includes(this.state.username) ?
                                        <Redirect to="/Ajay" />
                                        :
                                        (this.state.validName.includes(this.state.username) ?
                                            <Redirect to="/Neha" /> :
                                            <Redirect to="/Other" />)
                                    }
                                </Switch>
                            </Router>
                        </Login> :
                        <User >
                            <h1 >User Details:</h1>
                            <form >
                                <div>
                                    <label htmlFor="name">Userame:</label>

                                    <input type="text" onChange={this.handlerUserameChange} id="name" />
                                </div><br />
                                <div>
                                    <label htmlFor="email">Email:</label>
                                    <input type="email" id="email" />
                                </div>
                                <br />
                                <div>
                                    <label htmlFor="age">Age:</label>
                                    <input type="number" id="age" />
                                </div>
                                <br />
                                <div>
                                    <label htmlFor="gender">Gender:
                                <label htmlFor="male">Male</label>
                                        <input type="radio" name="Male" id="male" />
                                        <label htmlFor="female">Female</label>
                                        <input type="radio" name="Female" id="female" />
                                    </label>
                                </div>
                                <br />
                                <div>
                                    <label htmlFor="password">Password:</label>
                                    <input type="password" id="password" />
                                </div>
                                <br />
                                <div>
                                    <button type="submit" onClick={this.handleSubmit}>  Submit</button>

                                    <button type="reset">Reset</button>

                                </div>

                            </form><br />
                        </User>
                }

            </div>
        )
    }
}
export default Users;